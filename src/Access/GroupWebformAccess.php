<?php

namespace Drupal\group_webform\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Session\AccountInterface;
use Drupal\webform\WebformInterface;
use Drupal\webform\WebformSubmissionInterface;

/**
 * Checks access for displaying webform pages.
 */
class GroupWebformAccess implements GroupWebformAccessInterface {

  /**
   * {@inheritdoc}
   */
  public function webformEditAccess(AccountInterface $account, WebformInterface $webform = NULL) {
    if ($webform) {
      return \Drupal::service('groupwebform.webform')->webformAccess('update', $webform, $account);
    }
    return AccessResult::allowedIf($account->hasPermission('create webform'));
  }

  /**
   * {@inheritdoc}
   */
  public function webformDeleteAccess(AccountInterface $account, WebformInterface $webform) {
    return \Drupal::service('groupwebform.webform')->webformAccess('delete', $webform, $account);
  }

  /**
   * {@inheritdoc}
   */
  public function webformItemAccess(AccountInterface $account, WebformInterface $webform) {
    return \Drupal::service('groupwebform.webform')->webformAccess('view', $webform, $account);
  }

  /**
   * {@inheritdoc}
   */
  public function webformSubmissionAccess(AccountInterface $account, WebformSubmissionInterface $webform_submission) {
    return \Drupal::service('groupwebform.webform')->webformSubmissionAccessItems('view', $webform_submission, $account);
  }

  /**
   * {@inheritdoc}
   */
  public function webformSubmissionEditAccess(AccountInterface $account, WebformSubmissionInterface $webform_submission) {
    return \Drupal::service('groupwebform.webform')->webformSubmissionAccessItems('edit', $webform_submission, $account);
  }

  /**
   * {@inheritdoc}
   */
  public function webformSubmissionDeleteAccess(AccountInterface $account, WebformSubmissionInterface $webform_submission) {
    return \Drupal::service('groupwebform.webform')->webformSubmissionAccessItems('delete', $webform_submission, $account);
  }

  /**
   * {@inheritdoc}
   */
  public function webformSubmissionDownload(AccountInterface $account, WebformInterface $webform) {
    return \Drupal::service('groupwebform.webform')->webformAccess('view download', $webform, $account);
  }

}
