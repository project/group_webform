<?php

namespace Drupal\group_webform;

use Drupal\group\Entity\GroupContent;
use Drupal\webform\Utility\WebformDialogHelper;
use Drupal\webform\WebformSubmissionListBuilder;

/**
 * Provides a list controller for webform submission entity.
 *
 * @ingroup webform
 */
class GroupWebformSubmissionListBuilder extends WebformSubmissionListBuilder {

  /**
   * Build the webform submission entity list.
   *
   * @return array
   *   A renderable array containing the entity list.
   */
  protected function buildEntityList() {
    $build = [];

    // Filter form.
    if (empty($this->account)) {
      $build['filter_form'] = $this->buildFilterForm();
    }

    // Customize buttons.
    if ($this->customize) {
      $build['custom_top'] = $this->buildCustomizeButton();
    }

    // Display info.
    if ($this->total) {
      $build['info'] = $this->buildInfo();
    }

    // Table.
    $build += $this->renderWithAccess();
    $build['table']['#sticky'] = TRUE;
    $build['table']['#attributes']['class'][] = 'webform-results-table';

    // Customize.
    // Only displayed when more than 20 submissions are being displayed.
    if ($this->customize && isset($build['table']['#rows']) && count($build['table']['#rows']) >= 20) {
      $build['custom_bottom'] = $this->buildCustomizeButton();
      if (isset($build['pager'])) {
        $build['pager']['#weight'] = 10;
      }
    }

    // Must preload libraries required by (modal) dialogs.
    WebformDialogHelper::attachLibraries($build);

    return $build;
  }

  /**
   * Builds the entity listing as renderable array for table.html.twig.
   */
  public function renderWithAccess() {
    $plugin_id = 'group_webform:webform';
    $build['table'] = [
      '#type' => 'table',
      '#header' => $this->buildHeader(),
      '#title' => $this->getTitle(),
      '#rows' => [],
      '#empty' => $this->t('There are no @label yet.', ['@label' => $this->entityType->getPluralLabel()]),
      '#cache' => [
        'contexts' => $this->entityType->getListCacheContexts(),
        'tags' => $this->entityType->getListCacheTags(),
      ],
    ];
    foreach ($this->load() as $entity) {
      // Check submission access from source group.
      $source_group = $entity->getSourceEntity();
      if ($source_group instanceof GroupContent) {
        $source_group = $source_group->getGroup();
      }
      if ($this->currentUser->hasPermission('administer webform submission') ||
        ($source_group && $source_group->hasPermission("view $plugin_id submissions", $this->currentUser))) {
        if ($row = $this->buildRow($entity)) {
          $build['table']['#rows'][$entity->id()] = $row;
        }
      }
    }

    // Only add the pager if a limit is specified.
    if ($this->limit) {
      $build['pager'] = [
        '#type' => 'pager',
      ];
    }
    return $build;
  }

}
