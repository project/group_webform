<?php

namespace Drupal\group_webform;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\group\GroupMembershipLoader;
use Drupal\group\Entity\GroupContent;
use Drupal\webform\WebformInterface;
use Drupal\webform\WebformSubmissionInterface;

/**
 * Checks access for displaying webform pages.
 */
class GroupWebformService implements GroupWebformServiceInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The current user's account object.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The group membership loader.
   *
   * @var \Drupal\group\GroupMembershipLoader
   */
  protected $membershipLoader;

  /**
   * An array containing the webform access results.
   *
   * @var array
   */
  protected $webformAccess = [];

  /**
   * An array containing the webforms for a user.
   *
   * @var array
   */
  protected $userWebforms = [];

  /**
   * An array containing the webforms for a user and group.
   *
   * @var array
   */
  protected $userGroupWebforms = [];

  /**
   * Static cache of all group webform objects keyed by group ID.
   *
   * @var \Drupal\webform\WebformInterface[]
   */
  protected $groupWebforms = [];

  /**
   * Constructs a new GroupTypeController.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\group\GroupMembershipLoader $membership_loader
   *   The group membership loader.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, AccountInterface $current_user, GroupMembershipLoader $membership_loader) {
    $this->entityTypeManager = $entity_type_manager;
    $this->currentUser = $current_user;
    $this->membershipLoader = $membership_loader;
  }

  /**
   * {@inheritdoc}
   */
  public function webformAccess($op, WebformInterface $webform, AccountInterface $account = NULL) {
    if (!isset($account)) {
      $account = $this->currentUser;
    }

    if (isset($this->webformAccess[$op][$account->id()][$webform->id()])) {
      return $this->webformAccess[$op][$account->id()][$webform->id()];
    }

    if ($account->hasPermission('administer webform')) {
      return $this->webformAccess[$op][$account->id()][$webform->id()] = AccessResult::allowed();
    }

    $plugin_id = 'group_webform:webform';
    $group_content_types = $this->entityTypeManager->getStorage('group_content_type')
      ->loadByContentPluginId($plugin_id);
    if (empty($group_content_types)) {
      return $this->webformAccess[$op][$account->id()][$webform->id()] = AccessResult::neutral();
    }

    // Load all the group content for this webform.
    $group_contents = $this->entityTypeManager->getStorage('group_content')
      ->loadByProperties([
        'type' => array_keys($group_content_types),
        GroupContent::getEntityFieldNameForEntityType('webform') => $webform->id(),
      ]);

    // If the webform does not belong to any group, we have nothing to say.
    if (empty($group_contents)) {
      return $this->webformAccess[$op][$account->id()][$webform->id()] = AccessResult::neutral();
    }

    /** @var \Drupal\group\Entity\GroupInterface[] $groups */
    $groups = [];
    $content_webforms = [];
    foreach ($group_contents as $group_content) {
      /** @var \Drupal\group\Entity\GroupContentInterface $group_content */
      $group = $group_content->getGroup();
      $groups[$group->id()] = $group;
      $content_webforms[$group->id()][$webform->id()]['owner'] = $group_content->getOwner()->id();
    }

    // From this point on you need group to allow you to perform the requested
    // operation. If you are not granted access for a group, you should be
    // denied access instead.
    foreach ($groups as $group) {
      if ($op == 'update' || $op == 'delete') {
        if ($group->hasPermission("$op any $plugin_id entity", $account)) {
          return $this->webformAccess[$op][$account->id()][$webform->id()] = AccessResult::allowed();
        }
        elseif ($group->hasPermission("$op own $plugin_id entity", $account) && $content_webforms[$group->id()][$webform->id()]['owner'] == $account->id()) {
          return $this->webformAccess[$op][$account->id()][$webform->id()] = AccessResult::allowed();
        }
      }
      elseif ($group->hasPermission("$op $plugin_id entity", $account)) {
        return $this->webformAccess[$op][$account->id()][$webform->id()] = AccessResult::allowed();
      }
    }

    return $this->webformAccess[$op][$account->id()][$webform->id()] = AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  public function webformSubmissionAccessItems($op, WebformSubmissionInterface $webform_submission, AccountInterface $account = NULL) {
    if (!isset($account)) {
      $account = $this->currentUser;
    }
    $webform = $webform_submission->getWebform();
    if ($account->hasPermission('administer webform submission')) {
      return $this->webformAccess[$op][$account->id()][$webform_submission->id()] = AccessResult::allowed();
    }

    $plugin_id = 'group_webform:webform';
    $group_content_types = $this->entityTypeManager->getStorage('group_content_type')
      ->loadByContentPluginId($plugin_id);
    if (empty($group_content_types)) {
      return $this->webformAccess[$op][$account->id()][$webform->id()] = AccessResult::neutral();
    }

    // Load all the group content for this webform.
    $group_contents = $this->entityTypeManager->getStorage('group_content')
      ->loadByProperties([
        'type' => array_keys($group_content_types),
        GroupContent::getEntityFieldNameForEntityType('webform') => $webform->id(),
      ]);

    // If the webfotm does not belong to any group, we have nothing to say.
    if (empty($group_contents)) {
      return $this->webformAccess[$op][$account->id()][$webform->id()] = AccessResult::neutral();
    }
    // Check submission access from source group.
    $source_group = $webform_submission->getSourceEntity();
    if ($source_group->hasPermission("$op $plugin_id submissions", $account)) {
      return $this->webformAccess[$op][$account->id()][$webform->id()] = AccessResult::allowed();
    }

    return $this->webformAccess[$op][$account->id()][$webform->id()] = AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  public function loadUserOwnerGroupWebforms(AccountInterface $account = NULL) {
    if (!isset($account)) {
      $account = $this->currentUser;
    }
    if (isset($this->userWebforms[$account->id()])) {
      return $this->userWebforms[$account->id()];
    }

    $group_webforms = $this->getGroupWebforms($account->id());
    return $this->userWebforms[$account->id()] = $group_webforms;
  }

  /**
   * {@inheritdoc}
   */
  public function loadUserGroupWebformsByGroup($group_id, AccountInterface $account = NULL) {
    if (!isset($account)) {
      $account = $this->currentUser;
    }

    if (isset($this->userGroupWebforms[$account->id()][$group_id])) {
      return $this->userGroupWebforms[$account->id()][$group_id];
    }

    $group_webforms = $this->getGroupWebforms();
    $group_webform_for_group = (!empty($group_webforms[$group_id])) ? $group_webforms[$group_id] : [];

    return $this->userGroupWebforms[$account->id()][$group_id] = $group_webform_for_group;
  }

  /**
   * Get all group webform objects.
   *
   * We create a static cache of group webforms since loading them individually
   * has a big impact on performance.
   *
   * @return \Drupal\webform\WebformInterface[]
   *   A nested array containing all group webform objects keyed by group ID.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getGroupWebforms($owner_id = NULL) {
    if (!$this->groupWebforms) {
      $plugin_id = 'group_webform:webform';

      $webforms = $this->entityTypeManager->getStorage('webform')
        ->loadMultiple();

      $group_content_types = $this->entityTypeManager->getStorage('group_content_type')
        ->loadByContentPluginId($plugin_id);
      if (!empty($group_content_types)) {
        $properties = ($owner_id) ? ['type' => array_keys($group_content_types), 'uid' => $owner_id] : ['type' => array_keys($group_content_types)];
        $group_contents = $this->entityTypeManager->getStorage('group_content')
          ->loadByProperties($properties);

        foreach ($group_contents as $group_content) {

          // Getting the right entity id field.
          $group_content_field = $group_content->getEntityFieldName();
          // Make sure the group and entity IDs are set in the group content
          // entity.
          if (!isset($group_content->gid->target_id) || !isset($group_content->{$group_content_field}->target_id)) {
            continue;
          }
          /** @var \Drupal\group\Entity\GroupContentInterface $group_content */
          $this->groupWebforms[$group_content->gid->target_id][$group_content->{$group_content_field}->target_id] = $webforms[$group_content->{$group_content_field}->target_id];
        }
      }
    }
    return $this->groupWebforms;
  }

}
