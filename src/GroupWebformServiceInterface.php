<?php

namespace Drupal\group_webform;

use Drupal\webform\WebformSubmissionInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\webform\WebformInterface;

/**
 * Provides an interface defining a WebformAdmin manager.
 */
interface GroupWebformServiceInterface {

  /**
   * A custom access check for a webform operation.
   *
   * @param string $op
   *   The operation to perform on the webform.
   * @param \Drupal\webform\WebformInterface $webform
   *   Run access checks for this webform object.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Run access checks for this account.
   *
   * @return \Drupal\Core\Access\AccessResult
   *   The access result.
   */
  public function webformAccess($op, WebformInterface $webform, AccountInterface $account = NULL);

  /**
   * A custom access check for a webform submissions.
   *
   * @param string $op
   *   The operation to perform on the webform.
   * @param \Drupal\webform\WebformSubmissionInterface $webform_submission
   *   Run access checks for this webform object.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Run access checks for this account.
   *
   * @return \Drupal\Core\Access\AccessResult
   *   The access result.
   */
  public function webformSubmissionAccessItems($op, WebformSubmissionInterface $webform_submission, AccountInterface $account = NULL);

  /**
   * Load a list of webforms where a user is the owner.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user to load the webforms for.
   *
   * @return \Drupal\webform\WebformInterface[]
   *   An array of webform objects keyed by webform name.
   */
  public function loadUserOwnerGroupWebforms(AccountInterface $account = NULL);

  /**
   * Load a list of webforms for a group where a user can perform a operation.
   *
   * @param int $group_id
   *   The group ID to load the webforms from.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Run access checks for this account.
   *
   * @return \Drupal\webform\WebformInterface[]
   *   An array of webform objects keyed by webform name.
   */
  public function loadUserGroupWebformsByGroup($group_id, AccountInterface $account = NULL);

}
