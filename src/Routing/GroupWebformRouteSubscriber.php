<?php

namespace Drupal\group_webform\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Modify form for config.sync route.
 */
class GroupWebformRouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    $routes = $collection->all();
    foreach ($routes as $route_name => $route) {
      switch ($route_name) {
        case 'entity.webform.edit_form':
        case 'entity.webform.add_form':
        case 'entity.webform.settings':
        case 'entity.webform.settings_form':
        case 'entity.webform.settings_submissions':
        case 'entity.webform.settings_confirmation':
        case 'entity.webform.settings_assets':
        case 'entity.webform.settings_access':
        case 'entity.webform.handlers':
        case 'entity.webform.handler':
        case 'entity.webform.handler.add_form':
        case 'entity.webform.handler.add_email':
        case 'entity.webform.export_form':
        case 'entity.webform.test_form':
        case 'entity.webform_ui.element.edit_form':
        case 'entity.webform_ui.element':
        case 'entity.webform_ui.change_element':
        case 'entity.webform_ui.element.add_form':
        case 'entity.webform.source_form':
          $route->setRequirements(['_custom_access' => '\Drupal\group_webform\Access\GroupWebformAccess::webformEditAccess']);
          break;

        case 'entity.webform.delete_form':
        case 'entity.webform_ui.element.delete_form':
          $route->setRequirements(['_custom_access' => '\Drupal\group_webform\Access\GroupWebformAccess::webformDeleteAccess']);
          break;

        case 'entity.webform.results_submissions':
          $route->setRequirements(['_custom_access' => '\Drupal\group_webform\Access\GroupWebformAccess::webformItemAccess']);
          break;

        case 'entity.webform_submission.canonical':
        case 'entity.webform_submission.table':
        case 'entity.webform_submission.text':
        case 'entity.webform_submission.yaml':
          $route->setRequirements(['_custom_access' => '\Drupal\group_webform\Access\GroupWebformAccess::webformSubmissionAccess']);
          break;

        case 'entity.webform_submission.edit_form':
        case 'entity.webform_submission.edit_form.all':
          $route->setRequirements(['_custom_access' => '\Drupal\group_webform\Access\GroupWebformAccess::webformSubmissionEditAccess']);
          break;

        case 'entity.webform_submission.delete_form':
          $route->setRequirements(['_custom_access' => '\Drupal\group_webform\Access\GroupWebformAccess::webformSubmissionDeleteAccess']);
          break;

        case 'entity.webform.results_export':
          $route->setRequirements(['_custom_access' => '\Drupal\group_webform\Access\GroupWebformAccess::webformSubmissionDownload']);
          break;
      }
    }
  }

}
