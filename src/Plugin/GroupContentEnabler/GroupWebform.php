<?php

namespace Drupal\group_webform\Plugin\GroupContentEnabler;

use Drupal\group\Plugin\GroupContentEnablerBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a content enabler for webforms.
 *
 * @GroupContentEnabler(
 *   id = "group_webform",
 *   label = @Translation("Group webform"),
 *   description = @Translation("Adds webforms to groups both publicly and privately."),
 *   entity_type_id = "webform",
 *   entity_access = TRUE,
 *   pretty_path_key = "webform",
 *   reference_label = @Translation("Title"),
 *   reference_description = @Translation("The title of the webform to add to the group"),
 *   deriver = "Drupal\group_webform\Plugin\GroupContentEnabler\GroupWebformDeliver"
 * )
 */
class GroupWebform extends GroupContentEnablerBase {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    if (isset($form['use_creation_wizard'])) {
      $form['use_creation_wizard']['#access'] = FALSE;
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getPermissions() {
    $permissions = parent::getPermissions();

    $plugin_id = $this->getPluginId();

    // Allow permissions here and in child classes to easily use the plugin name
    // and target entity type name in their titles and descriptions.
    $t_args = [
      '%plugin_name' => $this->getLabel(),
      '%entity_type' => $this->getEntityType()->getLowercaseLabel(),
    ];
    $defaults = ['title_args' => $t_args, 'description_args' => $t_args];

    $permissions["view $plugin_id submissions"] = [
      'title' => " View %entity_type submissions",
    ] + $defaults;
    $permissions["edit $plugin_id submissions"] = [
      'title' => " Edit %entity_type submissions",
    ] + $defaults;

    $permissions["delete $plugin_id submissions"] = [
      'title' => " Delete %entity_type submissions",
    ] + $defaults;

    $permissions["view download $plugin_id entity"] = [
      'title' => "View download %entity_type submissions",
    ] + $defaults;

    return $permissions;
  }

}
