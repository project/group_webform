<?php

namespace Drupal\group_webform\Plugin\GroupContentEnabler;

use Drupal\Component\Plugin\Derivative\DeriverBase;

/**
 * Provides a webform deriver.
 */
class GroupWebformDeliver extends DeriverBase {

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $this->derivatives['webform'] = [
      'label' => t('Group webform'),
      'description' => t("Adds webform to groups"),
    ] + $base_plugin_definition;

    return $this->derivatives;
  }

}
