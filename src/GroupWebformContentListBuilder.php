<?php

namespace Drupal\group_webform;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\RedirectDestinationInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\group\Entity\Controller\GroupContentListBuilder;
use Drupal\group\Entity\GroupContentType;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a list controller for webforms entities in a group.
 */
class GroupWebformContentListBuilder extends GroupContentListBuilder {

  /**
   * The current user's account object.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, RedirectDestinationInterface $redirect_destination, RouteMatchInterface $route_match, EntityTypeInterface $entity_type, AccountInterface $current_user) {
    parent::__construct($entity_type_manager, $redirect_destination, $route_match, $entity_type);
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('redirect.destination'),
      $container->get('current_route_match'),
      $entity_type,
      $container->get('current_user')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEntityIds() {
    $plugin_id = 'group_webform:webform';
    $group_content_types = GroupContentType::loadByContentPluginId($plugin_id);

    // If we don't have any group webform plugins,
    // we don't have any group webform's.
    if (empty($group_content_types)) {
      return [];
    }

    $query = $this->getStorage()->getQuery();

    // Filter by group webform plugins.
    $query->condition('type', array_keys($group_content_types), 'IN');
    // Only show group content for the group on the route.
    $query->condition('gid', $this->group->id());

    // Only add the pager if a limit is specified.
    if ($this->limit) {
      $query->pager($this->limit);
    }

    $query->sort($this->entityType->getKey('id'));
    return $query->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header = [
      'id' => $this->t('ID'),
      'label' => $this->t('Webform'),
    ];
    $row = $header + parent::buildHeader();
    unset($row['entity_type'], $row['plugin']);
    return $row;
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\group\Entity\GroupContentInterface $entity */
    $row['id'] = $entity->id();
    $row['label']['data'] = $entity->getEntity()->toLink(NULL, 'edit-form');
    $row = $row + parent::buildRow($entity);
    unset($row['entity_type'], $row['plugin']);
    return $row;
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    $build = parent::render();
    $build['table']['#empty'] = $this->t("There are no webforms related to this group yet.");
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  protected function getDefaultOperations(EntityInterface $entity) {
    /** @var \Drupal\group\Entity\GroupContentInterface $entity */
    $operations = parent::getDefaultOperations($entity);
    $plugin_id = 'group_webform:webform';

    // Add view operation for the Group Content Relation.
    if (!isset($operations['view']) && $entity->access('view')) {
      $operations['view'] = [
        'title' => $this->t('View form'),
        'weight' => 1,
        'url' => $entity->toUrl(),
      ];
    }
    else {
      $operations['view']['title'] = $this->t('View webform entity');
    }

    // Slap on redirect destinations for the administrative operations.
    $destination = $this->redirectDestination->getAsArray();
    foreach ($operations as $key => $operation) {
      $operations[$key]['query'] = $destination;
    }

    if ($this->group->hasPermission("view $plugin_id entity", $this->currentUser)) {
      $operations['view-entity'] = [
        'title' => $this->t('View group webform'),
        'weight' => 1,
        'url' => $entity->toUrl(),
      ];
    }

    // Add operations to edit and delete the actual entity.
    if ($this->group->hasPermission("update any $plugin_id entity", $this->currentUser) || ($this->group->hasPermission("update own $plugin_id entity", $this->currentUser) && $entity->getOwner()->id() == $this->currentUser->id())) {
      $operations['edit-entity'] = [
        'title' => $this->t('Edit Webform'),
        'weight' => 102,
        'url' => $entity->getEntity()->toUrl('edit-form'),
      ];
    }
    if ($this->group->hasPermission("delete any $plugin_id entity", $this->currentUser) || ($this->group->hasPermission("delete own $plugin_id entity", $this->currentUser) && $entity->getOwner()->id() == $this->currentUser->id())) {
      $operations['delete-entity'] = [
        'title' => $this->t('Delete Webform'),
        'weight' => 103,
        'url' => $entity->getEntity()->toUrl('delete-form'),
      ];
    }
    return $operations;
  }

}
