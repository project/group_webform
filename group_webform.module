<?php

/**
 * @file
 * Gives the ability to create and manage webforms for groups.
 */

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\webform\Entity\Webform;

/**
 * Implements hook_help().
 */
function group_webform_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    case 'help.page.group_webform':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('This module allows you to create group_content webform entities for a group main entity.') . '</p>';
      return $output;

    default:
  }
}

/**
 * Implements hook_ENTITY_TYPE_delete().
 */
function group_webform_group_content_delete(EntityInterface $entity) {
  if ($entity->getContentPlugin()->getPluginId() === 'group_webform:webform') {
    \Drupal::service('cache.groupwebform')->invalidateAll();
  }
}

/**
 * Implements hook_entity_type_alter().
 */
function group_webform_entity_type_alter(array &$entity_types) {
  $entity_types['webform_submission']->setHandlerClass('list_builder', "Drupal\group_webform\GroupWebformSubmissionListBuilder");
}

/**
 * Prepares variables for the group content template to render the webform.
 *
 * Default template: group-content.html.twig.
 *
 * @param array $variables
 *   - elements: An array of elements to display in view mode.
 *   - group_content: The group content object.
 *   - view_mode: View mode; e.g., 'full', 'teaser', etc.
 */
function group_webform_preprocess_group_content(array &$variables) {
  /** @var \Drupal\webform\Entity\Webform $webform */
  $webform = $variables["group_content"]->getEntity();
  if ($webform instanceof Webform) {
    $output = \Drupal::EntityTypeManager()
      ->getViewBuilder('webform')
      ->view($webform);
    $variables["content"]["webform"] = $output;
  }
}
