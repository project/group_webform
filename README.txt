## CONTENTS OF THIS FILE

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


## INTRODUCTION

This module is designed to associate group specific webforms with a group when
using the Group module.


 * For a full description:
   https://drupal.org/project/group_webform

 * Issue queue for Group Webform:
   https://drupal.org/project/issues/group_webform

## REQUIREMENTS

 * Group module (http://drupal.org/project/group).
 * Webform module. (http://drupal.org/project/webform).


## INSTALLATION

  * Install normally as other modules are installed. For support:
    https://www.drupal.org/docs/8/extending-drupal/installing-contributed-modules
  * Enable the Group Webform plugin for a group type via:
   _/admin/group/types/manage/[GROUP TYPE]/content_

## CONFIGURATION

 * The configuration options for a group type are available via:
   _/admin/group/types/manage/[GROUP TYPE]/content_


## MAINTAINERS

Current maintainers:
 * Ivan Duarte (jidrone) - https://www.drupal.org/u/jidrone
 * Fabian Sierra (fabiansierra5191) - https://www.drupal.org/u/fabiansierra5191
